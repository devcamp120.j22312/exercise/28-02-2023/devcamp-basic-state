import { Component } from "react";

class Count extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
        // Cách 1:
        // this.handleClickMe = this.handleClickMe.bind(this);

    }
    // handleClickMe() {
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }

    handleClickMe = () => {
        this.setState({
            count: this.state.count + 1 
        })
    }
    render() {
        return(
            <>
            <p>Số lần click: {this.state.count}</p>
            <button onClick={this.handleClickMe}>Click me</button>
            </>
        )
    }
}

export default Count;